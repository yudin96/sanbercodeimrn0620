console.log("Soal No. 1 (Range) ");

function range(startNum, finishNum){
    var numbers = [];
    var par1 = 0;
    var par2 = 0;
    if(startNum ==null || finishNum ==null){
        return -1;
    }
    if(startNum >finishNum){
        par1=finishNum;
        par2=startNum;
    }else{
        par1=startNum;
        par2=finishNum;
    }
    
    for(var a = 0; (par1+a) <= par2; a++) {
       numbers.push(par1+a);
    } 

    if(startNum >finishNum){
        numbers.sort(function(a, b){return b-a});
  }

return numbers;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


console.log("Soal No. 2 (Range with Step)");
function rangeWithStep(startNum, finishNum, step) {
    var numbers = [];
    var numbers2 = [];
    var par1 = 0;
    var par2 = 0;
    if(startNum ==null || finishNum ==null){
        return -1;
    }
    if(startNum >finishNum){
        par1=finishNum;
        par2=startNum;
    }else{
        par1=startNum;
        par2=finishNum;
    }
    
    for(var a = 0; (par1+a) <= par2; a++) {
        numbers.push(par1+a);   
    } 
    
    var g=[];
    if(startNum > finishNum){
        numbers.sort(function(a, b){return b-a});
   }
  
    for(var a = 0; a < numbers.length; a++) {
        if(a%step==0){
            numbers2.push(numbers[a]);
        }
       
    }

    return numbers2;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log(" Soal No. 3 (Sum of Range) ")
function sum(startNum, finishNum, step) {
    var numbers = [];
    if (startNum!=null && finishNum!=null && step!=null){
        numbers=rangeWithStep(startNum,finishNum,step);
    }else if (startNum!=null && finishNum!=null){
        numbers=range(startNum,finishNum);
    }else if (startNum!=null){
        numbers.push(startNum);
    }else{
        numbers=0;
    }
    var jum=0;
    for(var a = 0; a < numbers.length; a++) {
        jum=jum+numbers[a];
       
    }
    return jum;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("Soal No. 4 (Array Multidimensi)");
function dataHandling(){
    console.log("----------------------------------");
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ] 

    for(var x = 0; x< input.length; x++){
        console.log("Nomor ID: "+input[x][0]);
        console.log("Nama Lengkap: "+input[x][1]);
        console.log("TTL: "+input[x][2]+input[x][3]);
        console.log("Hobi: "+input[x][4]);
        console.log("----------------------------------");
    }
return;
}
dataHandling();

console.log("Soal No. 5 (Balik Kata)");
function balikKata(x){

    var y = ""
    var a = 0;
    for(a =0; a<x.length;a++){
       var z = a;
    }
    for(var b = z; b >=0; b--){
        y +=x[b];
    }
    return y;
}
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));
    
console.log("Soal No. 6 (Metode Array)");
function dataHandling2(dataarr) {
    dataarr.splice(1, 1, dataarr[1]+" Elsharawy");
    dataarr.splice(2, 1, "Provinsi "+dataarr[2]);
    dataarr.splice(4, 2, "Pria","SMA Internasional Metro");
    console.log(dataarr);
    var tgl =(dataarr[3]).split('/')
    switch(tgl[1]) {
        case '01':   {
             console.log(' Januari '); 
             break; 
            }
        case '02':   {
            console.log(' Febuari '); 
            break; 
            }
        case '03':   {
            console.log(' Maret '); 
            break; 
            }
        case '04':   {
            console.log(' April '); 
            break; 
            }
        case '05':   {
            console.log(' Mei '); 
            break; 
            }
        case '06':   {
            console.log(' Juni '); 
            break; 
            }
        case '07':   {
            console.log(' Juli '); 
            break; 
            }
        case '08':   {
            console.log(' Agustus '); 
            break; 
            }
        case '09':   {
            console.log(' September '); 
            break; 
            }
        case '10':   {
            console.log(' Oktober '); 
            break; 
            }
        case '11':   {
            console.log('  Novenber '); 
            break; 
            }
        case '12':   {
            console.log('  Desember '); 
            break; 
            }
    
        }
        var tgl2 = tgl.join("-")
        tgl.sort(function(a, b){return b-a});
        console.log(tgl); 
        console.log(tgl2); 
        var irisan1 = (dataarr[1]).slice(0,14) 
        console.log(irisan1); 
    return dataarr;
}
//["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input)


// If-else
var nama = "yudin";
var peran = "penyihir";

if(nama == ""){
    console.log("Nama harus diisi!");
}else if(peran == ""){
    console.log(" Hallo "+nama+", Pilih Peranmu untuk memulai game");
}else{
    if(peran == "penyihir"){
        console.log("Selamat datang di Dunia penyihir, "+nama);
        console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
    }else if(peran == "guard"){
        console.log("Selamat datang di Dunia guard, "+nama);
        console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }else if(peran == "werewolf"){
        console.log("Selamat datang di Dunia werewolf, "+nama);
        console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!");
    }else{
        console.log("tidak ada peran")
    }
    
}

// Switch Case
var tanggal = 1; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2020; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

if((tanggal >= 1 && tanggal<= 31)&&(tahun >=1900 && tahun <= 2200) &&(bulan >=1 && bulan <=12)){
    switch(bulan) {
        case 1:
            console.log(tanggal+" January "+tahun);
            break;
        case 2:
            console.log(tanggal+" February "+tahun);
            break;
        case 3:
            console.log(tanggal+" Maret "+tahun);
            break;
        case 4:
            console.log(tanggal+" April "+tahun);
            break;
        case 5:
            console.log(tanggal+" Mei "+tahun);
            break;
        case 6:
            console.log(tanggal+" Juni "+tahun);
            break;
        case 7:
            console.log(tanggal+" Juli "+tahun);
            break;
        case 8:
            console.log(tanggal+" Agustus "+tahun);
            break;
        case 9:
            console.log(tanggal+" September "+tahun);
            break;
        case 10:
            console.log(tanggal+" Oktober "+tahun);
            break;
        case 11:
            console.log(tanggal+" November "+tahun);
            break;
        case 12:
            console.log(tanggal+" Desember "+tahun);
            break;
    }
}else{
    console.log("range tanggal tidak sesuai")
}

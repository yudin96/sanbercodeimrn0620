// Soal No. 1 (Membuat kalimat),
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
console.log(word+" "+second+" "+third+" "+fourth+" "+fifth+" "+sixth+" "+seventh);
console.log("=====================================================================================")
// Soal No.2 Mengurai kalimat (Akses karakter dalam string)
var xx = "I am going to be React Native Developer"; 
var firstWord= xx[0] ; 
var secondWord = xx[2] + xx[3]  ; 
var thirdWord = xx[5] + xx[6] + xx[7] + xx[8] + xx[9] ;
var fourthWord = xx[11] + xx[12]  ; 
var fifthWord = xx[14] + xx[15];
var sixthWord = xx[17] + xx[18] + xx[19] + xx[20] + xx[21];
var seventhWord = xx[23] + xx[24] + xx[25] + xx[26] + xx[27] + xx[28] ;
var eighthWord = xx[30] + xx[31] + xx[32] + xx[33] + xx[34] + xx[35] + xx[36] + xx[37] + xx[38] ;

console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)
console.log("=====================================================================================")
// Soal No. 3 Mengurai Kalimat (Substring)
var sentence2 = 'wow JavaScript is so cool'; 

var firstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25);

console.log('First Word: ' + firstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);
console.log("=====================================================================================")
// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
var sentence3 = 'wow JavaScript is so cool'; 

var firstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 25);

var firstWordLength = firstWord3.length 
var secondWordLength = secondWord3.length 
var thirdWordLength = thirdWord3.length 
var fourthWordLength = fourthWord3.length 
var fifthWordLength = fifthWord3.length  

console.log('First Word: ' + firstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength);

// First Word: wow, with length: 3 
// Second Word: JavaScript, with length: 10 
// Third Word: is, with length: 2 
// Fourth Word: so, with length: 2 
// Fifth Word: cool, with length: 4
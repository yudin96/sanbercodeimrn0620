import React from "react";

import { 
  StyleSheet, 
  View, 
  Image,
  TextInput,
  Button,
  Text,
  ScrollView
 } from 'react-native';

 const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

const LoginScreen = ({ navigation }) => (
      <ScreenContainer>
          <Image source={require('./images/logo.png')} style={{ marginVertical: 20, width: 300, height: 100, backgroundColor:'#3EC6FF' }} />
          <View style={styles.navItem}>
              <Text style={styles.titleText}>LOGIN</Text>
          </View>
          <ScrollView>
            
            <Text style={{fontSize:17, ontWeight:'bold'}}>UserName</Text>
            <TextInput style={{borderRadius: 8, width: 300, height:30, borderColor: 'black', borderWidth: 1 }}/>
            <Text style={{marginVertical: 10}}/>
            <Text style={{fontSize:17, ontWeight:'bold'}}>Password</Text>
            <TextInput style={{ placeholder:'Password', height:30, borderRadius: 8, width: 300, borderColor: 'black', borderWidth: 1 }}/>
            <View style={styles.buton}>
             <Button title="Login"  onPress={() => navigation.navigate('DrawerStackScreen')
              }/>
              <Text style={{marginVertical: 10, textAlign:'center'}}>Or</Text>
             <Button title="Create Account" onPress={ () =>  navigation.navigate('DaftarScreen')} />
            </View>
            
          </ScrollView>
      </ScreenContainer>
);
export default LoginScreen;

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor:"white",
      alignItems:'center',
  },
  titleText: {
    fontSize: 30,
    borderColor:'#3e3b63',
    marginVertical: 30,

  },
  buton:{
    marginVertical: 30,
    borderRadius: 8, 
    width: 300,
  },
  pass:{
    marginVertical: 30,
  }
})

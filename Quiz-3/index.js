import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import 'react-native-gesture-handler';

import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';

const Stack = createStackNavigator();
const RootStack = createStackNavigator();
const TabsStack = createBottomTabNavigator();
const DrawerStack = createDrawerNavigator();

const HomeStackScreen = () =>(
  <TabsStack.Navigator>
    <TabsStack.Screen name='LoginScreen' component={HomeScreen}
          options={{
              title: 'Login'
          }}></TabsStack.Screen>
  </TabsStack.Navigator>
);

const RootStackScreen = () => (
  <RootStack.Navigator>
      <RootStack.Screen name='LoginScreen' component={LoginScreen}
          options={{
              title: 'Login'
          }}
      />
      <RootStack.Screen name='HomeStackScreen' component={HomeStackScreen}
          options={{
              title: 'Home'
          }}
      />
  </RootStack.Navigator>
);

export default () => (
  <NavigationContainer>
      <RootStackScreen />
  </NavigationContainer>
);

// export default class App extends React.Component {
//   render() {
//     return (
//       <NavigationContainer>
//         <Stack.Navigator initialRouteName="Home" >
//           <Stack.Screen name='Login' component={LoginScreen}/>
//           <Stack.Screen name='Home' component={HomeScreen} options={{ headerTitle: 'Daftar Barang' }} />
//         </Stack.Navigator>
//       </NavigationContainer>
//     );
//   }
// }

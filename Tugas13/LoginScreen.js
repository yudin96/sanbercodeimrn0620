import React, { Component } from 'react';

import { 
  StyleSheet, 
  View, 
  Image,
  TextInput,
  Button,
  Text
 } from 'react-native';

export default class LogicScreen extends Component {
 
  render() {
    return (
      <View style={styles.container}>
          <Image source={require('./images/logo.png')} style={{ width: 300, height: 100, backgroundColor:'#3EC6FF' }} />
          <View style={styles.navItem}>
              <Text style={styles.titleText}>Register</Text>
          </View>
          <Text>UserName</Text>
          <TextInput style={{ width: 300, borderColor: 'black', borderWidth: 2 }}/>
          <View>
          </View>
          
          <Text>Password</Text>
          <TextInput style={{ width: 300, borderColor: 'black', borderWidth: 2 }}/>
          
          
          <Button title="Masuk ?" color="#3EC6FF"/>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 2,
      backgroundColor:"white",
      alignItems:'center',
      justifyContent: 'Space-around'
  },
  titleText: {
    fontSize: 30,
    borderColor:'#3e3b63',

  },
  navItem:{

  }
})
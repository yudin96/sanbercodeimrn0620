import React, { Component } from 'react';
import { FontAwesome5 } from '@expo/vector-icons';

import {
  StyleSheet,
  View,
  Image,
  TextInput,
  Button,
  Text
} from 'react-native';

export default class AboutScreent extends Component {

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.navItem}>
          <Text style={styles.titleText}>Tentang Saya</Text>
          <FontAwesome5 name='user-circle' size={200} color='#c0c0c0' solid />
          <Text style={{ fontSize: 20, color: '#003366', fontWeight: 'bold' }}>Nahyudin</Text>
          <Text style={{ color: '#2bbbdb' }}>React Native Developer</Text>

        </View>
        <View style={styles.navBar}>
          <Text style={{ alignItems: 'right', fontSize: 20, color: '#003366', fontWeight: 'bold' }}>Portofolio</Text>
          <View style={styles.kotakDalem}>
            <View style={{ width: 70}} >
              <FontAwesome5 name="gitlab" size={40} color="skyblue" />

              <Text style={{fontWeight:'bold'}}>@yudin96</Text>
            </View>

            <View style={{ width: 70}} >
            <FontAwesome5 name="github" size={40} color="skyblue" />
              <Text style={{fontWeight:'bold'}}>@yudin96</Text>
            </View>
          </View>
        </View>


        <View style={styles.navBar2}>
          <Text style={{ alignItems: 'right', fontSize: 20, color: '#003366', fontWeight: 'bold' }}>Hubungi Saya</Text>
          <View style={styles.kotakLuar}>
            <View style={{ width: 50, height: 50, alignItems:'center'}} >
              <View style={styles.kotakBawah}>
              <FontAwesome5 name="facebook" size={40} color="skyblue" />
              <Text style={{fontWeight:'bold'}}>  Yudin Nahyudin</Text>
              </View>
              <View style={styles.kotakBawah}>
              <FontAwesome5 name="instagram" size={40} color="skyblue"/>
              <Text style={{fontWeight:'bold'}}>  @nahyudin28</Text>
              </View>
              <View style={styles.kotakBawah}>
              <FontAwesome5 name="twitter" size={40} color="skyblue" />
              <Text style={{fontWeight:'bold', alignItems:'center', flexDirection: 'row',}}>  @yudin</Text>
              </View>
            </View>
          </View>
        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: "white",
    justifyContent: 'Space-around'
  },
  titleText: {
    fontSize: 36,
    color: '#003366',
    fontWeight: 'bold',
    alignItems: 'center',


  },
  navBar: {
    height: 100,
    backgroundColor: '#d3d3d3',
    elevation: 65,
    paddingHorizontal: 50,
    borderRadius: 16,
    lineHeight: 16,

  },

  navBar2: {
    height: 200,
    backgroundColor: '#d3d3d3',
    elevation: 65,
    paddingHorizontal: 50,
    borderRadius: 16,
    paddingTop: 20
  },
  navItem: {
    paddingTop: 50,
    alignItems: 'center',
  },
  kotakDalem: {
    borderTopWidth: 2,
    flexDirection: 'row',
    justifyContent: 'Space-around'
  },
  kotakLuar: {
    borderTopWidth: 2,
    flexDirection: 'row',
    justifyContent: 'Space-around'
  },
  kotakBawah:{
    width:150,
    flexDirection: 'row',
    justifyContent: 'Flex-start',
    alignItems:'right'
  }
})
import React from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { 
  View,
  Image,
  Text, 
  FlatList, 
  StyleSheet 
} from 'react-native';

import skillData from './skillData.json';
let Data = skillData.items;

export default class SkillScreen extends React.Component {

 render(){
   return(
     <View style={styles.container}>
       <View style={styles.header}>
            <View style={styles.headerAtas}>
                <Image source={require('./images/logo.png')} style={{ width: 190, height: 50, backgroundColor:'#3EC6FF' }} />
            </View>
            <View style={styles.headerBawah}>
                <MaterialIcons name="account-circle" size={40} color="skyblue" />
                <View>
                    <Text style={{color:'black'}}>  hal,</Text>
                    <Text style={{color:'#003366', fontSize:17}}>  Nahyudan</Text>
                </View>
            </View>
            <View style={styles.kotakSkill}>
            </View>
            <Text style={{marginHorizontal: 16, alignItems: 'right', fontSize: 40, color: '#003366' }}>Skill</Text>
            <View style={styles.kotakLuar}>
            </View>
       </View>
       
       <View></View>
       <View style={{marginHorizontal: 16, paddingTop:5, flex: 1, flexDirection: 'row', alignContent:'center', justifyContent: 'space-between'}}>
            <View style={{
                fontWeight: 'bold', 
                justifyContent: 'Space-around', 
                width: 135, 
                height: 35, 
                backgroundColor: '#B4E9FF', 
                borderRadius: 8, 
                alignItems:'center'
                }} >
                <Text style={{fontWeight:'bold',}}>{Data[0].categoryName}</Text>
            </View>
            <View style={{
                fontWeight: 'bold', 
                justifyContent: 'Space-around', 
                width: 150, 
                height: 35, 
                backgroundColor: '#B4E9FF', 
                borderRadius: 8, 
                alignItems:'center'
                }} >
                <Text style={{fontWeight:'bold',}}>{Data[2].categoryName}</Text>
            </View>
            <View style={{
                justifyContent: 'Space-around', 
                width: 70, 
                height: 35, 
                backgroundColor: '#B4E9FF', 
                borderRadius: 8, 
                alignItems:'center'
                }} >
                <Text style={{fontWeight:'bold',}}>{Data[4].categoryName}</Text>
            </View>
       </View>
        <View>
            <FlatList
            data={Data}
            renderItem={({ item }) => {
                let skillName = item.skillName;
                let percentageProgress = item.percentageProgress;
                let categoryName = item.categoryName;
                let logo = item.iconName;
                return (
                    <View style={styles.kotak}>
                        <MaterialCommunityIcons name={logo} size={90} color='darkblue' />
                        <Text> </Text>
                        <View style={styles.itemText}>
                            <Text  style={{fontWeight:'bold', fontSize: 25,  color:'darkblue', textAlign:'left'}}>{skillName}</Text>
                            <Text style={{fontWeight:'bold', fontSize: 15,  color:'#3EC6FF'}}>{categoryName}</Text>
                            <Text style={{fontWeight:'bold', fontSize: 30,  color:'white', textAlign:'right'}}>{percentageProgress}</Text>
                        </View>
                        <FontAwesome5 name="chevron-right" size={80} color="darkblue" />
                    </View>
                )
            }}
            >
            </FlatList> 
           
        </View>
     </View>
   );
 }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor:'white',
  },
  headerAtas: {
    width: 187,
    height: 51,
    left: 187,
  },
  headerBawah: {
    flexDirection: 'row',
    marginTop:10,
    marginHorizontal: 16,
  },
  kotakLuar: {
    borderTopWidth: 4,
    flexDirection: 'row',
    justifyContent: 'Space-around',
    borderTopColor:'#B4E9FF',
    marginHorizontal: 16,
  },
  kotakSkill: {
    height: 5,
    backgroundColor: '#white',
    marginHorizontal: 16,
  },
  kotak: {
    backgroundColor: '#B4E9FF',
    // backgroundColor:'black',
    padding: 25,
    marginVertical: 5,
    marginHorizontal: 16,
    borderRadius: 8,
    justifyContent:'space-between',
    flexDirection:'row',
    alignItems: 'stretch',

  },
  itemText: {
    width:200,
    marginVertical: 5,
  }
});


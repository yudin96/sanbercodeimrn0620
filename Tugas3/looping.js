console.log("No. 1 Looping While");

console.log("LOOPING PERTAMA");
var a = 1;
while(a <= 20) { 
    if(a % 2==0){
        console.log(a+" - I love coding"); 
    }
 
  a++; 
}

console.log("LOOPING KEDUA");
var a = 20;
while(a >= 1) { 
    if(a % 2==0){
        console.log(a+" - I love coding"); 
    }
 
  a--; 
}

console.log("No. 2 Looping menggunakan for");
for(let b=1; b<20; b++){
    if(b%3==0 & b%2==1){
        console.log(b+" - I Love Coding");
    }else if(b%2==0){
        console.log(b+" - Berkualitas");
    }else if(b%2==1){
        console.log(b+" - Santai");
    }else{
        
    }
}

console.log("No. 3 Membuat Persegi Panjang");
var p =" ";
for(var x = 0;x<8; x++){
    p +="#";
    
}
for(var y = 0;y<4; y++){
    console.log(p);
}


console.log("No. 4 Membuat Tangga");
var s="";
for(var a = 1; a<=7; a++){
    for(var b = 1; b<2; b++){
        s +="#";
        // console.log(s)
    }
    console.log(s)
   
}


console.log("No. 5 Membuat Papan Catur");
var p1 =" ";
var p2 =" ";
var p3 =" ";
var p4 =" ";
var p5 =" ";
var p6 =" ";
var p7 =" ";
var p8 =" ";

for(var x = 1;x<=8; x++){
    if(x%2==0){
        p1+="#";
        p2+=" ";
        p3+="#";
        p4+=" ";
        p5+="#";
        p6+=" ";
        p7+="#";
        p8+=" ";
    }else{
        p1+=" ";
        p2+="#";
        p3+=" ";
        p4+="#";
        p5+=" ";
        p6+="#";
        p7+=" ";
        p8+="#";
    }
}
console.log(p1);
console.log(p2);
console.log(p3);
console.log(p4);
console.log(p5);
console.log(p6);
console.log(p7);
console.log(p8);
